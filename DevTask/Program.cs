﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevTask
{
    public interface IAccountService
    {
        double GetAccountAmount(int accountId);
    }
    
    public class AccountInfo
    {
        //Fields
        private readonly int _accountId;
        private readonly IAccountService _accountService;
        //property implementation
        public double Amount { get; private set; }

        //Constructor
        public AccountInfo (int accountId, IAccountService accountService)
        {
            _accountId = accountId;
            _accountService = accountService;
        }
        
        public void RefreshAmount()
        {
            Amount = _accountService.GetAccountAmount(_accountId);
        }
    }

    public class AccService : IAccountService
    {
        public double GetAccountAmount(int accountId)
        {
            return (double)(accountId + accountId);
        }
    }

     class Test
    {
        static void Main(string[] args)
        {
            IAccountService ai = new AccService();
            AccountInfo a = new AccountInfo(3,ai );
            //AccountInfo a1 = new AccountInfo(3, ai);
            Console.WriteLine(a.Amount);
            a.RefreshAmount();

            //a1.RefreshAmount();
            Console.WriteLine(a.Amount);
            Console.ReadKey();
        }

        
    }
}
