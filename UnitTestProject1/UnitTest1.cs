﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            //Arrange
            int expected = 24;

            //Act
            DevTask.IAccountService a1 = new DevTask.AccService();
            DevTask.AccountInfo acc = new DevTask.AccountInfo(12, a1);
            acc.RefreshAmount();

            //Assert
            Assert.AreEqual(expected, acc.Amount);

        }
    }
}
